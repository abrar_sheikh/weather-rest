package zappos.weather.web.dto;

import java.util.List;

import zappos.weather.web.dataProviders.WeatherDataService;
import zappos.weather.web.dto.models.Weather;
import zappos.weather.web.dto.models.WeatherForcast;

public class WeatherService {
	public static Weather getByCity(final String city, final String dataProvider)
			throws Exception {
		final WeatherDataService wds = WeatherDataServiceFactory
				.getDataService(dataProvider);
		return wds.getByCity(city);
	}

	public static List<WeatherForcast> getForcastByCity(final String city,
			final int count, final String dataProvider) throws Exception {
		final WeatherDataService wds = WeatherDataServiceFactory
				.getDataService(dataProvider);
		return wds.getForcastByCity(city, count);
	}
}
