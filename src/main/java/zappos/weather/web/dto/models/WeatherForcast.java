package zappos.weather.web.dto.models;


import java.io.Serializable;

public class WeatherForcast implements Serializable {
	private long id;
	private String main;
	private String description;
	private long dt;
	private String min;
	private String max;

	public long getId() {
		return id;
	}

	public void setId(final long id) {
		this.id = id;
	}

	public String getMain() {
		return main;
	}

	public void setMain(final String main) {
		this.main = main;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public long getDt() {
		return dt;
	}

	public void setDt(final long dt) {
		this.dt = dt;
	}

	public String getMin() {
		return min;
	}

	public void setMin(final String min) {
		this.min = min;
	}

	public String getMax() {
		return max;
	}

	public void setMax(final String max) {
		this.max = max;
	}

	public WeatherForcast() {
		super();
	}

	public WeatherForcast(final long id, final String main,
			final String description, final long d, final String string,
			final String string2) {
		super();
		this.id = id;
		this.main = main;
		this.description = description;
		dt = d;
		min = string;
		max = string2;
	}

}
