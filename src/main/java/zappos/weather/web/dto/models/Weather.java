package zappos.weather.web.dto.models;


import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Foo")
public class Weather implements Serializable {
	private long id;
	private String main;
	private String description;
	private String country;
	private String city;
	private double temp;
	private long dt;

	public Weather() {
		super();
	}

	public Weather(final long id, final String main, final String description,
			final String country, final String city, final double temp,
			final long dt) {
		super();

		this.id = id;
		this.main = main;
		this.description = description;
		this.country = country;
		this.city = city;
		this.temp = temp;
		this.dt = dt;
	}

	// API

	public long getId() {
		return id;
	}

	public void setId(final long id) {
		this.id = id;
	}

	public String getMain() {
		return main;
	}

	public void setMain(final String main) {
		this.main = main;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		main = description;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(final String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(final String city) {
		this.city = city;
	}

	public double getTemp() {
		return temp;
	}

	public void setTemp(final double temp) {
		this.temp = temp;
	}

	public long getDt() {
		return dt;
	}

	public void setDt(final long dt) {
		this.dt = dt;
	}

}