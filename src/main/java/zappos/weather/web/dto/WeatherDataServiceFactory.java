package zappos.weather.web.dto;

import zappos.weather.web.dataProviders.WeatherDataService;
import zappos.weather.web.dataProviders.openweathermaps.OpenWeatherDataService;

public class WeatherDataServiceFactory {
	public static WeatherDataService getDataService(final String service) {
		if (service == "openweathermpas")
			return (WeatherDataService) new OpenWeatherDataService();
		else
			return null;
	}
}
