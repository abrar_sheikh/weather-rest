package zappos.weather.web.dataProviders;

import java.util.List;

import zappos.weather.web.dto.models.Weather;
import zappos.weather.web.dto.models.WeatherForcast;

public interface WeatherDataService {
	public Weather getByCity(final String city) throws Exception;

	public List<WeatherForcast> getForcastByCity(final String city,
			final int count) throws Exception;
}
