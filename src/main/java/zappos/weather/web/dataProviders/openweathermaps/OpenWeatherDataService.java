package zappos.weather.web.dataProviders.openweathermaps;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import zappos.weather.web.dataProviders.APIAdaptor;
import zappos.weather.web.dataProviders.WeatherDataService;
import zappos.weather.web.dto.models.Weather;
import zappos.weather.web.dto.models.WeatherForcast;

public class OpenWeatherDataService implements WeatherDataService {
	final String getByCityURL = "http://api.openweathermap.org/data/2.5/weather?q=%s";
	final String getForcastByCityURL = "http://api.openweathermap.org/data/2.5/forecast/daily?q=%s&mode=json&units=metric&cnt=%d";

	@Override
	public Weather getByCity(final String city) throws Exception {
		Object res = null;

		try {
			res = APIAdaptor.get(String.format(getByCityURL, city));
		} catch (final Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return parseWeatherObj((JSONObject) res);
	}

	@Override
	public List<WeatherForcast> getForcastByCity(final String city,
			final int count) throws Exception {
		Object res = null;

		try {
			res = APIAdaptor.get(String
					.format(getForcastByCityURL, city, count));
		} catch (final Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(res);
		return parseWeatherCollectionObj((JSONObject) res);
	}

	private Weather parseWeatherObj(final JSONObject obj) throws Exception {
		checkApiCod(obj);
		final JSONObject sys = (JSONObject) obj.get("sys");
		final JSONObject main = (JSONObject) obj.get("main");
		final JSONArray weatherArr = (JSONArray) obj.get("weather");
		final JSONObject weatherObj = (JSONObject) weatherArr.get(0);

		return new Weather((long) weatherObj.get("id"),
				(String) weatherObj.get("main"),
				(String) weatherObj.get("description"),
				(String) sys.get("country"), (String) obj.get("name"),
				(double) Math.round((double) main.get("temp") - 273),
				(long) obj.get("dt"));
	}

	private List<WeatherForcast> parseWeatherCollectionObj(final JSONObject res)
			throws Exception {
		checkApiCod(res);
		final JSONArray resArr = (JSONArray) res.get("list");
		final List<WeatherForcast> wfArr = new ArrayList<WeatherForcast>();
		JSONObject obj = null;
		WeatherForcast wf = null;

		JSONObject temp = null;
		JSONArray weatherArr = null;
		JSONObject weatherObj = null;
		for (int j = 0; j < resArr.size(); j++) {
			obj = (JSONObject) resArr.get(j);
			temp = (JSONObject) obj.get("temp");
			weatherArr = (JSONArray) obj.get("weather");
			weatherObj = (JSONObject) weatherArr.get(0);
			wf = new WeatherForcast((long) weatherObj.get("id"),
					(String) weatherObj.get("main"),
					(String) weatherObj.get("description"),
					(long) obj.get("dt"), temp.get("min").toString(), temp.get(
							"max").toString());
			wfArr.add(wf);
		}
		return wfArr;
	}

	private void checkApiCod(final JSONObject obj) throws Exception {
		String msg = null;
		if (obj.containsKey("cod")
				&& !obj.get("cod").toString().trim().equals("200")) {
			System.out.println("exception");
			msg = (obj.containsKey("message") == true ? obj.get("message")
					.toString() : "Error");
			throw new Exception(msg);
		}
	}
}
