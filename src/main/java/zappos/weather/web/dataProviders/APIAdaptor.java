package zappos.weather.web.dataProviders;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class APIAdaptor {
	@SuppressWarnings("finally")
	public static Object get(final String URL) throws Exception {
		try {

			final URL url = new URL(URL);
			final HttpURLConnection conn = (HttpURLConnection) url
					.openConnection();
			final JSONParser parser = new JSONParser();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}

			final BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));

			String output = "";
			String temp = "";
			while ((temp = br.readLine()) != null) {
				output += temp;
			}
			conn.disconnect();

			return parser.parse(output);

		} catch (final MalformedURLException e) {

			throw new Exception(e.getMessage());

		} catch (final IOException e) {

			throw new Exception(e.getMessage());

		} catch (final ParseException e) {
			// TODO Auto-generated catch block
			throw new Exception(e.getMessage());
		}
	}
}

