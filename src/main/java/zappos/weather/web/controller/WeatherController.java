package zappos.weather.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import zappos.weather.web.dto.WeatherService;
import zappos.weather.web.dto.models.Weather;
import zappos.weather.web.dto.models.WeatherForcast;

@Controller
public class WeatherController {

	public WeatherController() {
		super();
	}

	// API - read

	@RequestMapping(method = RequestMethod.GET, value = "/getTempByCity/{city}")
	@ResponseBody
	public Weather getTempByCity(@PathVariable final String city)
			throws Exception {
		return WeatherService.getByCity(city, "openweathermpas");
	}

	@RequestMapping(method = RequestMethod.GET, value = "/getTempForcastByCity/{city}/{count}")
	@ResponseBody
	public List<WeatherForcast> getTempForcastByCity(
			@PathVariable final String city, @PathVariable final Integer count)
					throws Exception {
		return WeatherService.getForcastByCity(city, count, "openweathermpas");
	}

	@ExceptionHandler(Exception.class)
	public @ResponseBody String handleEmployeeNotFoundException(
			final HttpServletRequest request, final Exception ex) {
		System.out.println(ex.getMessage());
		return ex.getMessage();
	}

}
