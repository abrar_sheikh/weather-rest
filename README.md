Author
======
Abrar Ahmed Sheikh
aas847@nyu.edu

Weather Rest
============
Goal of this project is to provide current weather report for a city and given a city provide weather forcast.

Note: class diagram can be found in the project root

API's
=====
/getTempByCity/{city} : get temparature for a given city eg /getTempByCity/new+york,us or /getTempByCity/dubai

/getTempForcastByCity/{city}/{count} : get weather forecast for a given city eg /getTempForcastByCity/new+york/5 this will return forecast for next 5 days in newyork.

Furture Work
============
- Write Test cases for apis
- Write Exception Class for handle different kinds of errors.
- Currently has only one data provider openweathermaps, can load more dataproviders and some mock data for testing and options at api level to select custom data provider

